       IDENTIFICATION  DIVISION. 
       PROGRAM-ID. DATA5.
       AUTHOR. ONGART
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  GRADE-DATA PIC X(90) VALUE "63160083ONGART          886345593
      -    "B 886352593D+886342193B+886478593C 886481592C+886491591A". 
       01  GRADE.
           03 STU-ID PIC 9(8).
           03 STU-NAME PIC X(16).
           03 SUB1.
              05 CODE1 PIC  9(8).
              05 NAME1 PIC  9.
              05 SUB-GRADE1 PIC  X(2).
           03 SUB2.   
              05 CODE2 PIC  9(8).
              05 NAME2 PIC  9.
              05 SUB-GRADE2 PIC  X(2).
           03 SUB3.
              05 CODE3 PIC  9(8).
              05 NAME3 PIC  9.
              05 SUB-GRADE3 PIC  X(2).
           03 SUB4.   
              05 CODE4 PIC  9(8).
              05 NAME4 PIC  9.
              05 SUB-GRADE4 PIC  X(2).
           03 SUB5.
              05 CODE5 PIC  9(8).
              05 NAME5 PIC  9.
              05 SUB-GRADE5 PIC  X(2).
           03 SUB6.
              05 CODE6 PIC  9(8).
              05 NAME6 PIC  9.
              05 SUB-GRADE6 PIC  X(2).
       66 STUDENT-ID RENAMES STU-ID.
       66 STUDENT-INFO RENAMES STU-ID THRU STU-NAME.
       01 STUCODE REDEFINES  GRADE.
           05 STU-YEAR PIC 9(2).
           05 FILLER PIC X(6).
           05 STU-SHORT-NAME PIC X(3). 

       
       PROCEDURE DIVISION.
       BEGIN.
           MOVE GRADE-DATA TO GRADE 
           DISPLAY GRADE 
           DISPLAY "SUBJECT 1"
           DISPLAY "CODE: " CODE1
           DISPLAY "UNIT: " NAME1 
           DISPLAY "GRADE: " SUB-GRADE1 

           DISPLAY "SUBJECT 2"
           DISPLAY "CODE: " CODE2 
           DISPLAY "UNIT: " NAME2
           DISPLAY "GRADE: " SUB-GRADE2

           DISPLAY "SUBJECT 3"
           DISPLAY "CODE: " CODE3
           DISPLAY "UNIT: " NAME3 
           DISPLAY "GRADE: " SUB-GRADE3 

           DISPLAY "SUBJECT 4"
           DISPLAY "CODE: " CODE4
           DISPLAY "UNIT: " NAME4 
           DISPLAY "GRADE: " SUB-GRADE4 

           DISPLAY "SUBJECT 5"
           DISPLAY "CODE: " CODE5
           DISPLAY "UNIT: " NAME5
           DISPLAY "GRADE: " SUB-GRADE5

           DISPLAY "SUBJECT 6"
           DISPLAY "CODE: " CODE6
           DISPLAY "UNIT: " NAME6 
           DISPLAY "GRADE: " SUB-GRADE6 

           DISPLAY SUB4

           DISPLAY STUDENT-ID
           DISPLAY STUDENT-INFO

           DISPLAY STU-YEAR STU-SHORT-NAME 
           .


       